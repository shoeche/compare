#include "My_File.H"

#include "Smart_Pointer.C"
#include "Message.H"

using namespace ATOOLS;

namespace ATOOLS{
  template class SP(std::ifstream);
  template class SP(std::ofstream);
}

std::ostream &ATOOLS::operator<<(std::ostream &ostr,const fom::code &code)
{
  if (code&fom::permanent) ostr<<"[permanent]";
  if (code&fom::open) ostr<<"[open]";
  if (code&fom::error) ostr<<"[error]";
  return ostr;
}
	
namespace ATOOLS {
	
  template <> std::ostream &
  operator<<<std::ifstream>(std::ostream &ostr,
			    const My_File<std::ifstream> &file)
  {
    return ostr<<"("<<(&*file)<<") [input] { m_path = "<<file.Path()
	       <<", m_file = "<<file.File()
	       <<", m_mode = "<<file.Mode()<<" }";
  }

  template <> std::ostream &
  operator<<<std::ofstream>(std::ostream &ostr,
			    const My_File<std::ofstream> &file)
  {
    return ostr<<"("<<(&*file)<<") [output] { m_path = "<<file.Path()
	       <<", m_file = "<<file.File()
	       <<", m_mode = "<<file.Mode()<<" }";
  }

}

template <class FileType>
inline My_File<FileType>::My_File(): 
  p_file(NULL), m_mode(fom::permanent) {}

template <class FileType>
inline My_File<FileType>::~My_File() 
{ 
}

template <class FileType>
FileType *My_File<FileType>::operator()() const 
{ 
  return --p_file; 
}

template <class FileType>
FileType *My_File<FileType>::operator->() const 
{ 
  return --p_file; 
}

template <class FileType>
FileType &My_File<FileType>::operator*() const  
{ 
  return *p_file;  
}

template <class FileType>
bool My_File<FileType>::Open(const int mode) 
{ 
  if (m_path=="" && m_file=="") return false;
  Close();
  p_file = new File_Type();
  if (mode&1) p_file->open((m_path+m_file).c_str(),std::ios::app);
  else p_file->open((m_path+m_file).c_str());
  return p_file->good();

}

template <class FileType>
bool My_File<FileType>::Close()
{
  if (p_file==NULL) return false;
  p_file->close();
  p_file=NULL;
  return true;
}

template <class FileType>
void My_File<FileType>::SetPath(const std::string &path) 
{
  if (path!=m_path) Close();
  m_path=path; 
}

template <class FileType>
void My_File<FileType>::SetFile(const std::string &file) 
{ 
  if (file!=m_file) Close();
  m_file=file; 
}

template <class FileType>
void My_File<FileType>::SetMode(const fom::code &mode) 
{
  m_mode=mode; 
}

template <class FileType>
const std::string &My_File<FileType>::Path() const 
{ 
  return m_path; 
}

template <class FileType>
const std::string &My_File<FileType>::File() const 
{ 
  return m_file; 
}

template <class FileType>
const fom::code &My_File<FileType>::Mode() const 
{ 
  return m_mode; 
}

template class My_File<std::ifstream>;
template class My_File<std::ofstream>;

