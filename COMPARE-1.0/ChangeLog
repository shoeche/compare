Changes in version 1.9

	- new options 'DEFAULT_FONT', 'LEG_FONT' and 'FONT'
	- general overhaul of internal interpreter
	- several bug fixes

Changes in version 1.8

	- simultaneous tex and html file generation
	  with links to png and eps files on webpage
	- commandline option '-b' changes, see man page
	- new option '-N' for histogram location display
	- tag setting throughout input files
	- several bug fixes

Changes in version 1.7

	- new automatic plot range determination
	  corresponding tags 'Y_MIN_STRETCH' and 
	  'Y_MAX_STRETCH' for customization
	- tag overriding via '@@ !' or '?? !', see info pages
	- backward compatibility with TAG_REPLACE and 
	  GLOBAL_TAG_REPLACE dropped
	- new reader for analytic results.
	- several bug fixes
	
Changes in version 1.6

	- new data sorting
	  input histograms are sorted w.r.t. x-coordinates
	- modified tag replacement
	  global tags set using '??' or 'GLOBAL_TAG_REPLACE'
	- specification of plot range for histograms
	  lower/upper edge set via 'CUTOFF_MIN'/'CUTOFF_MAX'
	- new tags to set frame fill style & colour
	- revised tag syntax
	  more intuitive new tags while keeping full
	  backward compatibility, see info pages
	  new shorthand of 'READER_PARAMETERS' -> '##'
	- several bug fixes
	
Changes in Version 1.5

	- restructured tex source output
	- possibility to do setup in one single file
	  via command line options '-i' and '-I'
	- new add-on reader for mcfm output
	- several bug fixes

Changes in Version 1.4

	- new treatment of conditionals, 'else' fields 
	  and nested conditionals are supported
	- accelerated reading of input files
	- new shorthand of 'TAG_REPLACE' 
	  to reduce setup size -> '@@'
	- several bug fixes

Changes in Version 1.3

	- new web page option
	  output can be created into an html document 
	  with arbitrary header, tail and title
	- several bug fixes

Changes in Version 1.2

	- completely dynamic library loading
	  system starts up with executeable from 'Main.C' 
	  and tools only
	- extendable with custom readers and configurators 
	  dynamically loaded, no source code or library 
	  modification required; loading steered
	  by the environment variable 'COMPARE_LDADD'
	- revised algebra reader
	  arbitrary formula possible, weights in data sets
	  are accessed via 'y[<set number>]', x value via 'x'
	- new auto configuration tool
	  steered by auto configuration input file 
	  'autoconfig.dat' see info pages for instructions
	
