#ifndef Root_Histogram_Reader_H
#define Root_Histogram_Reader_H

#include "Histogram_Reader_Base.H"

namespace COMPARE {

  class Root_Histogram_Reader: public Histogram_Reader_Base {
  private:

    std::string m_key;

  public:

    // constructor
    Root_Histogram_Reader(Histogram_Handler_Base *handler);

    // destructor
    ~Root_Histogram_Reader();

    // member functions
    bool ReadData();

  };// end of class Root_Histogram_Reader

}// end of namespace COMPARE

#endif
