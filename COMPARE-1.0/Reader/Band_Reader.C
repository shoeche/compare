#include "Band_Reader.H"

#include "MathTools.H"
#include "Message.H" 
#include "MyStrStream.H" 
#include "My_Limits.H"

using namespace COMPARE;
using namespace ATOOLS;

DECLARE_GETTER(Band_Reader_Getter,"BAND",
	       Histogram_Reader_Base,Histogram_Handler_Base *const);

Histogram_Reader_Base *Band_Reader_Getter::operator()
  (Histogram_Handler_Base *const &handler) const
{ 
  return new Band_Reader(handler);
}

void Band_Reader_Getter::
PrintInfo(std::ostream &str,const size_t width) const
{ 
  str<<"error band"; 
}

Band_Reader::Band_Reader(Histogram_Handler_Base *handler):
  Histogram_Reader_Base(handler), m_master(0), m_mode(0)
{
  std::string key(handler->GetReaderKey());
  Algebra_Interpreter ai;
  if (key.length()) m_mode=ToType<int>(ai.Interprete(key));
}

Band_Reader::~Band_Reader() 
{
}

bool Band_Reader::CalculatePoints()
{
  double def(0.0);
  Data_Reader reader(" ",";","//");
  reader.AddWordSeparator("\t");
  reader.SetString(reader.StripEscapes((*p_info)[sic::reader_parameters]));
  if (!reader.ReadFromString(def,"DEFAULT_Y_VALUE")) def=0.0;
  m_values.clear();
  DataType &mdata(m_dmode&1?*m_readers[m_master]->AllData():
		  *m_readers[m_master]->Data());
  size_t nmbins(mdata.front().size());
  double l(0.0), r(0.0);
  for (size_t j(0);j<nmbins;++j) {
    if (mdata.size()>3 && mdata[2][j]>0.0 && mdata[3][j]>0.0) {
      l=mdata[0][j]-mdata[2][j];
      r=mdata[0][j]+mdata[3][j];
    }
    else {
      l=mdata[0][j];
      r=j<nmbins-1?mdata[0][j+1]:2.0*mdata[0][j]-mdata[0][j-1];
    }
    std::vector<double> data(m_readers.size(),def);
    for (size_t i(0);i<m_readers.size();++i) 
      if (m_readers[i]!=NULL) data[i]=CollectBins(i,l,r);
    m_values.push_back(data);
    (*p_data)[0].push_back((l+r)/2.0);
    (*p_data)[1].push_back(0.0);
    (*p_data)[2].push_back((r-l)/2.0);
    (*p_data)[3].push_back((*p_data)[2].back());
    (*p_data)[4].push_back(0.0);
    (*p_data)[5].push_back(0.0);
  }
  return true;
}

double Band_Reader::CollectBins
(const size_t &ci,const double &left,const double &right) const
{
  double sum(0.0), dist(0.0), leftx(0.0), rightx(0.0);
  DataType &mdata(m_dmode&2?*m_readers[ci]->AllData():
		  *m_readers[ci]->Data());
  size_t ncbins(mdata.front().size());
  for (unsigned int i=0;i<ncbins;++i) {
    if (mdata.size()>3 && mdata[2][i]>0.0 && mdata[3][i]>0.0) {
      leftx=mdata[0][i]-mdata[2][i];
      rightx=mdata[0][i]+mdata[3][i];
    }
    else {
      leftx=mdata[0][i];
      rightx=i<ncbins-1?mdata[0][i+1]:2.0*mdata[0][i]-mdata[0][i-1];
    }
    double l(Max(leftx,left)), r(Min(rightx,right));
    if (l<r) {
      dist+=r-l;
      sum+=mdata[m_columns[ci]-1][i]*(r-l);
    }
  }
  sum=sum/dist;
  if (!(sum>0.0) && !(sum<=0.0)) return 0.0;
  return sum;
}

bool Band_Reader::ReadData() 
{
  if (Handler()->Files().size()<1 &&
      Handler()->Paths().size()<1) return false;
  size_t histograms=Min(Handler()->Paths().size(),
			Handler()->Files().size());
  Data_Reader reader(" ",";","//");
  reader.AddWordSeparator("\t");
  reader.SetString(reader.StripEscapes((*p_info)[sic::reader_parameters]));
  if (!reader.VectorFromString(m_columns,"COLUMNS"))
    m_columns=std::vector<unsigned int>(histograms,2);
  if (!reader.ReadFromString(m_dmode,"BAND_DATA")) m_dmode=0;
  int helpi(0), miss(1);
  if (reader.ReadFromString(helpi,"BAND_MASTER")) m_master=helpi;
  if (m_master>histograms) m_master=0;
  else if (m_master>0) --m_master;
  std::string helps;
  if (reader.ReadFromString(helps,"ALLOW_MISSING")) miss=helps=="YES";
  msg_Info()<<METHOD<<"(): Adding files for band with m_dmode="<<m_dmode<<"\n";
  for (unsigned int i=0;i<histograms;++i) {
    msg_Info()<<"   y"<<i<<" -> '"<<Handler()->Paths()[i]
	      <<Handler()->Files()[i]<<"'("<<m_columns[i]<<") "
	      <<(m_master==i?'*':' ')<<"\n";
  }
  Histogram_Reader_Base *single;
  for (unsigned int i=0;i<histograms;++i) {
    std::string singlename=Handler()->Paths()[i]+Handler()->Files()[i];
    single=GetReader(NULL,singlename);
    m_readers.push_back(single);
    if (single==NULL) {
      msg_Info()<<"Band_Reader::ReadData(): Histogram '"
		<<singlename<<"' not found."<<std::endl;
      if (!miss) return false;
    }
    else {
      if ((m_dmode&2?single->AllData():single->Data())->size()<m_columns[i]) {
	msg_Info()<<METHOD<<"(): '"<<singlename<<"'->"
		  <<(m_dmode&2?"All":"")<<"Data()->size() = "
		  <<single->AllData()->size()<<" < "<<m_columns[i]<<std::endl;
	if (!miss) return false;
	m_readers.back()=NULL;
      }
      if (m_master==i&&
	  (m_dmode&1?single->AllData():single->Data())->size()<m_columns[i]) {
	msg_Info()<<METHOD<<"(): '"<<singlename<<"'->"
		  <<(m_dmode&1?"All":"")<<"Data()->size() = "
		  <<single->AllData()->size()<<" < "<<m_columns[i]<<std::endl;
	if (!miss) return false;
	m_readers.back()=NULL;
      }
    }
  }
  if (m_readers[m_master]==NULL) {
    for (unsigned int i=0;i<histograms;++i)
      if (m_readers[m_master=i]!=NULL) break;
    if (m_readers[m_master]==NULL) {
      msg_Info()<<METHOD<<"(): No input. Abort.\n";
      return false;
    }
  }
  p_data->resize(6);
  if (!CalculatePoints()) return false;
  if (!Calculate()) return false;
  std::string name="("+ToString(this)+")";
  s_readermap[name]=this;
  msg_Tracking()<<"Band_Reader_Base::ReadData(): "
		<<"Created histogram name {\n"
	        <<"   '"<<name<<"'\n}"<<std::endl;
  m_values.clear();
  p_alldata = new std::vector<std::vector<double> >(*p_data);
  return true;
}

bool Band_Reader::Calculate()
{
  for (m_cur=0;m_cur<m_values.size();++m_cur) {
    (*p_data)[1][m_cur]=m_values[m_cur][m_master];
    if (m_mode==0) {
      double sum(0.0);
      for (size_t i(0);i<m_values[m_cur].size();++i)
	sum+=dabs(m_values[m_cur][i]-m_values[m_cur][m_master]);
      (*p_data)[5][m_cur]=(*p_data)[4][m_cur]=sum;
    }
    else if (m_mode==1) {
      double sum2(0.0);
      for (size_t i(0);i<m_values[m_cur].size();++i)
	sum2+=sqr(m_values[m_cur][i]-m_values[m_cur][m_master]);
      (*p_data)[5][m_cur]=(*p_data)[4][m_cur]=sqrt(sum2);
    }
    else if (m_mode==2) {
      if (m_values[m_cur].size()%2!=1) {
	msg_Error()<<METHOD<<"(): Invalid number of histograms."<<std::endl;
      }
      else {
	double sum2p(0.0), sum2m(0.0);
	for (size_t i(1);i<m_values[m_cur].size();i+=2) {
	  double s1(m_values[m_cur][i]-m_values[m_cur][0]);
	  double s2(m_values[m_cur][i+1]-m_values[m_cur][0]);
	  sum2p+=sqr(Max(0.0,Max(s1,s2)));
	  sum2m+=sqr(Min(0.0,Min(s1,s2)));
	}
	(*p_data)[4][m_cur]=sqrt(sum2m);
	(*p_data)[5][m_cur]=sqrt(sum2p);
      }
    }
    else if (m_mode==3) {
      double cmin((*p_data)[1][m_cur]), cmax((*p_data)[1][m_cur]);
      for (size_t i(0);i<m_values[m_cur].size();++i) {
	cmin=Min(cmin,m_values[m_cur][i]);
	cmax=Max(cmax,m_values[m_cur][i]);
      }
      (*p_data)[4][m_cur]=(*p_data)[1][m_cur]-cmin;
      (*p_data)[5][m_cur]=cmax-(*p_data)[1][m_cur];
    }
    else if (m_mode==13) {
      if (m_values[m_cur].size()%2!=1) {
	msg_Error()<<METHOD<<"(): Invalid number of histograms."<<std::endl;
      }
      double cmin1((*p_data)[1][m_cur]), cmax1((*p_data)[1][m_cur]);
      for (size_t i(0);i<m_values[m_cur].size()/2+1;++i) {
	cmin1=Min(cmin1,m_values[m_cur][i]);
	cmax1=Max(cmax1,m_values[m_cur][i]);
      }
      double cmin2((*p_data)[1][m_cur]), cmax2((*p_data)[1][m_cur]);
      for (size_t i(m_values[m_cur].size()/2+1);i<m_values[m_cur].size();++i) {
	cmin2=Min(cmin2,m_values[m_cur][i]);
	cmax2=Max(cmax2,m_values[m_cur][i]);
      }
      (*p_data)[4][m_cur]=((*p_data)[1][m_cur]-cmin1)+((*p_data)[1][m_cur]-cmin2);
      (*p_data)[5][m_cur]=(cmax1-(*p_data)[1][m_cur])+(cmax2-(*p_data)[1][m_cur]);
    }
    else {
      THROW(fatal_error,"Invalid mode: "+ToString(m_mode));
    }
    if (msg_LevelIsTracking()) {
      msg_Tracking()<<"  x = "<<std::setw(15)<<(*p_data)[0][m_cur]
		    <<", y = "<<std::setw(15)<<(*p_data)[1][m_cur]
		    <<" + "<<std::setw(15)<<(*p_data)[4][m_cur]
		    <<" - "<<std::setw(15)<<(*p_data)[5][m_cur]<<"\n";
    }
  }
  return true;
}

