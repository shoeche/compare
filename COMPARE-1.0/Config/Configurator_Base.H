#ifndef Configurator_Base_H
#define Configurator_Base_H

#include "Getter_Function.H"
#include "Data_File.H"

namespace COMPARE {

  class Configurator_Base {
  public:

    typedef ATOOLS::Getter_Function<Configurator_Base,Data_File *const> 
    Getter_Function;

  protected:

    Data_File *p_root;

    std::string m_name;

    double m_eymin, m_eymax;

    std::string Escape(const std::string &input) const;
    std::string ShellName(const std::string &input) const;


  public:

    // constructor
    Configurator_Base(Data_File *const paths);

    // destructor
    virtual ~Configurator_Base();

    // member functions
    virtual bool Configure(std::ostream &ostr,const bool diffplot);

    virtual void Reset();

    // inline functions
    inline void SetEYMin(const double &eymin) { m_eymin=eymin; }
    inline void SetEYMax(const double &eymax) { m_eymax=eymax; }

    inline void SetName(const std::string &name) { m_name=name; }

  };// end of class Configurator_Base

}// end of namespace COMPARE

#endif
